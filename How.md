# what

自定义yii2 基础模板

# how 1 创建项目

* 为了可以方便合并yiisoft/yii2-app-basic最新修改，保持可以更新到最新的模板
* 所以使用如下方法创建自己的基础模板
* 参考 https://www.yiichina.com/doc/guide/2.0/tutorial-start-from-scratch

```bash

git clone git@github.com:yiisoft/yii2-app-basic.git 233-yii2-app-basic
cd 233-yii2-app-basic
git remote add upstream git@github.com:yiisoft/yii2-app-basic.git
git remote set-url origin git@gitlab.com:do365-public/233-yii2-app-basic.git

# 主要是为了可以执行如下操作

git fetch upstream
git merge upstream/master

```

### how 2 发布到packagist.org

* 登录 https://packagist.org
* submit `https://gitlab.com/do365-public/233-yii2-app-basic.git`
* submit时一定要填https地址，要不Details链接无法访问
* 提交时 A package with the name yiisoft/yii2-app-basic already exists.
* 所以需要先修改 compose.json中的name为 do365/233-yii2-app-basic

### how 3 发布检查

* https://packagist.org/packages/do365/233-yii2-app-basic

### how 4 自动发布到 packagist.org

* gitlab do365-public 233-yii2-app-basic Settings Integrations Packagist
* 输入 username token
* 查看 username https://packagist.org/profile/edit
* 查看 token https://packagist.org/profile/


### how 5 安装依赖 更新依赖(可选)

```
composer install --no-interaction --ansi
composer update --no-interaction --ansi
```

### how 6 创建数据库()

```
mysql -uroot -e "drop database if exists yii2basic"
mysql -uroot -e "CREATE DATABASE IF NOT EXISTS yii2basic default charset utf8 COLLATE utf8_unicode_ci;"
```

### how 7 启动程序

```
 ./yii serve -p 8089

```
* 访问 http://localhost:8089/
* 以 admin/admin登录

### how 8 创建数据迁移

```
# 创建表 news
./yii migrate/create create_news_table --interactive=0
```
* 开发点1 完善迁移脚本
运行迁移
```
./yii migrate/up --interactive=0
```

### how 9 运行测试

```
mysql -uroot -e "drop database if exists yii2_basic_tests"
mysql -uroot -e "CREATE DATABASE IF NOT EXISTS yii2_basic_tests default charset utf8 COLLATE utf8_unicode_ci;"
tests/bin/yii migrate --interactive=0
brew install selenium-server-standalone
brew services start selenium-server-standalone
./yii serve -p 8089
vendor/bin/codecept run
```

