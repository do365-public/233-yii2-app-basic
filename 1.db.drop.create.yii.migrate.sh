#!/usr/bin/env bash

#set -x

export CMD_PATH=$(cd `dirname $0`; pwd)
cd $CMD_PATH

mysql -uroot -e "drop database if exists yii2basic"
mysql -uroot -e "CREATE DATABASE IF NOT EXISTS yii2basic default charset utf8 COLLATE utf8_unicode_ci;"

./yii migrate/up --interactive=0