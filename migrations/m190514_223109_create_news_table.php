<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 * 参考 https://www.yiichina.com/doc/guide/2.0/db-migrations
 */

class m190514_223109_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'content' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}
